#!/usr/bin/bash
build () {
  mkdir -p build
  cd build
  cmake ..
  make
  cd ..
}

simulate () {
  mkdir -p $1
  cd $1
  ../build/micro_aevol_cpu -g $2 -w $3 -h $4 -m $5 -s 1234
  cd ..
}

full_simulation() {
  TIMEFORMAT=%R

  mkdir -p $1
  cd $1

  GENOME=(500 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000)
  POPULATION=(32 64 128 256 512 1024)
  MUTATION=(0.0001 0.00001 0.000001)
  echo "sim_folder;time;genome_size;population_size;mutation_rate" >> record.csv


  for (( i=0; i<${#GENOME[@]}; i++ ))
  do
    mkdir -p "sim_1_${i}"
    cd "sim_1_${i}"
    C_TIME=$(time (../../build/micro_aevol_cpu -g ${GENOME[i]} -w ${POPULATION[0]} -h ${POPULATION[0]} -m ${MUTATION[0]} >out.log 2>&1) 2>&1)
    cd ..
    echo "sim_1_${i}; $C_TIME; ${GENOME[i]}; ${POPULATION[0]}; ${MUTATION[0]}" >> record.csv

  done

  for (( i=0; i<${#POPULATION[@]}; i++ ))
  do
    mkdir -p "sim_2_${i}"
    cd "sim_2_${i}"
    C_TIME=$(time (../../build/micro_aevol_cpu -g ${GENOME[0]} -w ${POPULATION[i]} -h ${POPULATION[i]} -m ${MUTATION[0]}  >out.log 2>&1) 2>&1)
    cd ..
    echo "sim_2_${i}; $C_TIME; ${GENOME[0]}; ${POPULATION[i]}; ${MUTATION[0]}" >> record.csv

  done

  for (( i=0; i<${#MUTATION[@]}; i++ ))
  do
    mkdir -p "sim_3_${i}"
    cd "sim_3_${i}"
    C_TIME=$(time (../../build/micro_aevol_cpu -g ${GENOME[0]} -w ${POPULATION[0]} -h ${POPULATION[0]} -m ${MUTATION[i]} >out.log 2>&1) 2>&1)
    cd ..
    echo "sim_3_${i}; $C_TIME; ${GENOME[0]}; ${POPULATION[0]}; ${MUTATION[i]}" >> record.csv

  done
}

"$@"
